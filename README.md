# Angular Web Component Serve CLI
At smallstack we develop web components and plug them into our backoffice applications. Instead of replicating our applications & microservices for local development, we want to reference our local web components online in our applications, hence this CLI.

## How does it work?
An express server serves all angular files (runtime.js, polyfills.js, styles.js, scripts.js, vendor.js, main.js) as one concatinated file at http://localhost:4250/webcomponent. This endpoint now gets proxied via ngrok to a random ngrok address which can be used in our applications. Websockets are proxied as well, so live-mode is possible.

## Installation
`npm i -g @smallstack/ng-wc-serve`

Have a look at the inline help: `ng-wc-serve --help`. Since version 1.1.x ngrok is disabled by default and has to be enabled via `--ngrok`

# Endpoints
- /webcomponent: serves files as concatinated file
- /health: returns 200 OK (e.g. if used with https://github.com/bahmutov/start-server-and-test)

## Guide
1. To make the concatination process work, you need to run your angular dev server in production mode. At least you have to set `optimization=true` and `sourceMap=false` during the ngcc process, otherwise you'll run into js file parsing issues in your browser (`Cannot parse <`). We also need to disable the host check when ngrok is used, otherwise it won't work. Create a new script called `start:wc` in your angular project and add the following content: `"ng serve --optimization=true --sourceMap=false [--disableHostCheck=true --no-live-reload]"`. Adjust the command as much as you want to, e.g. set a project or additional build parameters.
2. Start the CLI (in any folder) via `ng-wc-serve`, you'll see something like this: ![Command Line Screenshot](https://smallstack-cdn.s3.eu-central-1.amazonaws.com/screenshots/ngwcserve.png)
3. Follow the instructions and start the Angular Dev Server as described
4. Now you can copy the ngrok address and get started

## Debugging
Debugging your your web component can be hard since the code is minified. If you turn of angular's optimizations, this whole setup won't work anymore. Instead, you can turn off several build switches and still create a production bundle: 

- NG_BUILD_MANGLE=false
- NG_BUILD_MINIFY=false
- NG_BUILD_BEAUTIFY=false
