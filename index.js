#!/usr/bin/env node
const Express = require("express");
const http = require("http");
const httpProxy = require("http-proxy");
const cors = require("cors");
const ngrok = require("ngrok");
const yargs = require("yargs");

const argv = yargs
  .options({
    "angular-dev-server-port": { type: "number", default: 4200, alias: "p" },
    includeStyles: { type: "boolean", default: false },
    includePolyfills: { type: "boolean", default: false },
    includeVendor: { type: "boolean", default: true },
    includeScripts: { type: "boolean", default: true },
    ngrok: { type: "boolean", default: false },
  })
  .help("h")
  .alias("h", "help").argv;

const angularDevServerPort = argv["angular-dev-server-port"];
const includeStyles = argv.includeStyles;
const includePolyfills = argv.includePolyfills;
const includeVendor = argv.includeVendor;
const includeScripts = argv.includeScripts;
const useNgrok = argv.ngrok;
const angularDevServerUrl = "http://localhost:" + angularDevServerPort;
const server = Express();
server.use(cors());
let proxy = httpProxy.createProxyServer({
  target: angularDevServerUrl,
  ws: true,
});

server.get("/webcomponent", async (req, res) => {
  try {
    let script = "";
    script += await getJs(angularDevServerUrl + "/runtime.js");
    if (includePolyfills === true)
      script += await getJs(angularDevServerUrl + "/polyfills.js");
    if (includeStyles === true)
      script += await getJs(angularDevServerUrl + "/styles.js");
    if (includeScripts === true)
      script += await getJs(angularDevServerUrl + "/scripts.js");
    if (includeVendor === true)
      script += await getJs(angularDevServerUrl + "/vendor.js");
    script += await getJs(angularDevServerUrl + "/main.js");
    res.end(script);
  } catch (e) {
    res.end("Error while getting files: " + JSON.stringify(e));
    console.error(e);
  }
});
server.get("/health", (req, res) => {
  res.end("OK");
});
server.all("/sockjs-node/*", (req, res) => {
  try {
    proxy.web(req, res);
  } catch (e) {
    res.end(e);
    console.error(e);
  }
});
server.on("upgrade", (req, socket, head) => {
  proxy.ws(req, socket, head);
});
server.listen(4250);

server.on("error", (err) => {
  console.error(err);
});
process.on("uncaughtException", (err) => {
  console.error(err);
});

if (useNgrok) {
  (async () => {
    const url = await ngrok.connect({
      proto: "http",
      addr: 4250,
      region: "eu",
      onStatusChange: (status) => {
        console.log("NGROK: ", status);
      },
    });

    console.log(
      "\n###########################################################################"
    );
    console.log("## Serving webcomponent at " + url + "/webcomponent");
    console.log(
      "###########################################################################"
    );
    console.log(
      "\n==> Please open another console and start the angular dev server via :"
    );
    const additionalCommands =
      angularDevServerPort === 4200 ? "" : " --port=" + angularDevServerPort;
    console.log(
      "\n\tnpm run start:wc -- --no-live-reload --publicHost=" +
        url +
        additionalCommands
    );
    console.log("\nor in an nx workspace one of these:");
    console.log(
      "\n\tnx serve webcomponents --prod --no-live-reload -- --publicHost=" +
        url +
        additionalCommands
    );
    console.log(
      "\n\tnx serve webcomponents --configuration=sandbox --no-live-reload  -- --publicHost=" +
        url +
        additionalCommands
    );
  })();
} else {
  console.log(
    "\n###########################################################################"
  );
  console.log("## Serving webcomponent at http://localhost:4250/webcomponent");
  console.log(
    "###########################################################################"
  );
  console.log(
    "\n==> Please open another console and start the angular dev server via :"
  );
  const additionalCommands =
    angularDevServerPort === 4200 ? "" : " --port=" + angularDevServerPort;
  console.log("\n\tnpm run start:wc" + additionalCommands);
  console.log("\nor in an nx workspace one of these:");
  console.log(
    "\n\tnx serve webcomponents --prod --liveReload=false" + additionalCommands
  );
  console.log(
    "\n\tnx serve webcomponents --configuration=sandbox --liveReload=false" +
      additionalCommands
  );
}

function getJs(url) {
  try {
    return new Promise((resolve, reject) => {
      const req = http
        .get(url, (res) => {
          if (res.statusCode !== 200) {
            throw new Error(
              "Got HTTP " +
                res.statusCode +
                " while trying to get " +
                url +
                ", is the angular dev server running?"
            );
          }
          let data = "";
          res.on("data", (chunk) => {
            data += chunk;
          });
          res.on("end", () => {
            resolve(data);
          });
        })
        .on("error", (e) => {
          console.error(
            "Error while getting " +
              url +
              ", is the angular dev server running?"
          );
          reject(e);
        })
        .end();
    });
  } catch (e) {
    console.error(
      "Error while getting " + url + ", is the angular dev server running?"
    );
    throw e;
  }
}
